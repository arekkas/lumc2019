% Created 2019-12-05 Thu 10:24
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{tabularx}
\usepackage{subfig}
\usetheme{metropolis}
\author{Alexandros Rekkas}
\date{05-12-2019}
\title{Assessing heterogeneity of treatment effect in a network of databases}
\graphicspath{{./img/}}
\hypersetup{
 pdfauthor={Alexandros Rekkas},
 pdftitle={Assessing heterogeneity of treatment effect in a network of databases},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}


\section{Heterogeneity of treatment effect}
\label{sec:org92e314d}
\begin{frame}[label={sec:org8d1abf7}]{Heterogeneity of treatment effect (HTE)}
Heterogeneity of treatment effect refers to the nonrandom variation in the magnitude or direction of a treatment effect across levels of a covariate, as measured on a selected scale, against a clinical outcome.
\end{frame}

\section{Review of the literature}
\label{sec:orgd22dfc4}
\begin{frame}[label={sec:org20f22ad}]{Sources}
\begin{itemize}
\item Medline and Cochrane central for the time period of 01-01-2000 through 08-09-2018
\item Seminal articles suggested by a technical expert panel
\item References of eligible studies
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org4da9516}]{Inclusion criteria}
Papers that:
\begin{itemize}
\item Develop or evaluate methods of predictive HTE in the setting of parallel arm RCT designs or simulated RCTs
\item Describe a generic approach that can be applied using both regression and non-regression methods
\item Compare the generic one-variable-at-a-time approach to a predictive HTE approach
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org491864b}]{Exclusion criteria 1}
Papers that:
\begin{itemize}
\item Are related to cross-over, single-arm or observational study designs. Trial enrichment or adaptive trial designs were also excluded.
\item Apply predictive HTE only to address clinical aims
\item Use only non-regression based methods
\item Only consider the traditional one-variable-at-a-time approach
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org74193b3}]{Exclusion criteria 2}
Papers that (continued):
\begin{itemize}
\item Primarily aim at characterization or identification of heterogeneity in response
\item Use covariates post-baseline
\item Are review articles
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org6bc9a73}]{Flow chart}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/studyFlowChart.JPG}
\caption{Study flow chart}
\end{figure}
\end{frame}

\begin{frame}[label={sec:orgc3e715a}]{Risk based methods}
\begin{itemize}
\item Risk-based methods exploit the mathematical dependency of treatment benefit on a patient’s baseline risk
\item Even though relative treatment effect may vary across different levels of baseline risk, relative treatment effect modification by each covariate is not considered.
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orga1b6b27}]{Risk based methods}
Given a multivariate risk prediction model for outcome \(y\) from predictors \(x_1,\dots,x_p\)
\begin{equation*}
risk(x_1,\dots,x_p) = E\{y|x_1,\dots,x_p\}=f(\alpha+\beta_1x_1+\dots+\beta_px_p)
\end{equation*}
The expected outcome of a patient receiving treatment \(T\) based on the linear predictor \(lp(x_1,\dots,x_p)=\alpha+\beta_1x_1+\dots+\beta_px_p\) from a previously derived risk model can be described as:
\begin{itemize}
\item Assuming constant relative treatment effect
\end{itemize}
\begin{equation}
E\{y|x_1,\dots,x_p,T\}=f(\beta_{lp}lp + \gamma_0T)
\end{equation}
\begin{itemize}
\item Allowing treatment effect to vary along the distribution of risk:
\end{itemize}
\begin{equation}
E\{y|x_1,\dots,x_p,T\}=f(\beta_{lp}lp + \gamma_0T+\gamma Tlp)
\end{equation}
\end{frame}
\begin{frame}[label={sec:orgc40146a}]{Risk based methods}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/riskModelingDemo.JPG}
\end{center}
\end{frame}
\begin{frame}[label={sec:org5e54651}]{Treatment effect modeling}
Treatment effect modeling methods use both the main effects of risk factors and covariate-by-treatment interaction terms (on the relative scale) to estimate individualized benefits.
\end{frame}
\begin{frame}[label={sec:org60f947a}]{Treatment effect modeling}
The expected outcome of a patient with measured predictors \(x_1,\dots,x_p\) receiving treatment T, is derived from:

\begin{equation*}
E\{y|x_1,\dots,x_p,T\}=f(\alpha+\beta_1x_1+\dots+\beta_px_p+\gamma_1x_1T+\dots+\gamma_px_pT)
\end{equation*}
\end{frame}
\begin{frame}[label={sec:org6ed1e24}]{Optimal treatment regimes}
\begin{itemize}
\item Treatment assignment rule dividing the trial population into those who benefit from treatment and those who do not
\item Focus primarily on treatment effect modifiers
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orgd831ae9}]{Optimal treatment regimes}
A treatment regime \(T(x_1,\dots,x_p)\) is a binary treatment assigment rule based on measured predictors. The optimal treatment regime maximizes the overall expected outcome across the entire target population:

\begin{equation*}
T_{optimal}=argmax_TE\big\{E\{y|x_1,\dots,x_p,T(x_1,\dots,x_p)\}\big\}
\end{equation*}
\end{frame}
\begin{frame}[label={sec:org172c2a6}]{Model evaluation methods}
\begin{itemize}
\item Extensive literature on model evaluation in prediction modeling
\item Harder when modeling treatment effects (unavailable counterfactual outcomes)
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org5d548fd}]{Results}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/litRevRes.jpg}
\caption{Publications included in the review from 1999 until 2019}
\end{figure}
\end{frame}

\begin{frame}[label={sec:orgc9eac95}]{Future work}
\begin{itemize}
\item Determine optimal approaches to the reduction of overfitting through penalization.
\item Measures to evaluate models intended to predict treatment benefit.
\item General principles to judge the adequacy of sample sizes.
\item Methods that simultaneously predict multiple risk dimensions regarding both primary outcome risks and treatment-related harms
\end{itemize}
\end{frame}
\section{OHDSI}
\label{sec:org8de8c03}
\begin{frame}[label={sec:org302a188}]{OHDSI}
OHDSI:
\begin{itemize}
\item is a multi-stakeloder, interdisciplinary collaborative aiming to bring out the value of health data through large-scale analytics
\item has established a international network of researchers and observational health databases with a coordinating center housed at Columbia university
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org4354fcb}]{OHDSI collaborators}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/ohdsiCollaborators.JPG}
\caption{OHDSI collaborators around the world}
\end{figure}
\end{frame}
\begin{frame}[label={sec:org43e10b6}]{OHDSI data}
Data across the OHDSI community:
\begin{itemize}
\item 133 different databases with patient-level data from various perspectives
\begin{itemize}
\item Electronic health records
\item Administrative claims
\item Hospital systems
\item Clininical registries
\item Health surveys
\item Biobanks
\end{itemize}
\item More than 0.5 billion patient records
\begin{itemize}
\item Data in 18 different countries
\item More than 369 million records outside the US
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orge1cc403}]{Common data model}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/cdm.JPG}
\end{center}
\end{frame}
\begin{frame}[label={sec:org76dfa49}]{Methods library}
\begin{center}
\includegraphics[width=200px]{./img/methodsLibrary.JPG}
\end{center}
\end{frame}

\section{A framework for risk based assessment of HTE in a network of databases}
\label{sec:orga9d4aed}
\begin{frame}[label={sec:orge8aef91}]{Step 1: Problem definition}
\alert{\alert{Cohort}} is the set of persons within a specific database that satisfy one or more inclusion criteria for a duration of time.

\begin{itemize}
\item Treatment cohort
\item Comparator cohort
\item Outcome cohrort(s)
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org066215f}]{Step 2: Identification of the database}
\begin{itemize}
\item \alert{\alert{Aim}}: Apply the framework to a large set of databases.
\end{itemize}

However:
\begin{itemize}
\item Defined cohorts may not be populated
\item Adequate sample sizes
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orge3bdcd4}]{Step 3: Prediction}
\begin{itemize}
\item Either external or internal prediction models blinded to treatment assignment can be used
\item Adopt the OHDSI prediction framework:
\begin{itemize}
\item Define target cohort by merging treatment and comparator cohorts
\item Define outcome cohort(s)
\item Define the target population
\item Define the time at risk
\item Select the set of covariates relevant for prediction
\item Select the prediction algorithm
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org5606e61}]{Step 4: Estimation}
\begin{itemize}
\item Derive target population
\item Define time at risk
\item Apply the prediction model on the target population
\item Stratify the target population
\item Estimate treatment effects within risk strata
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orge211f7c}]{Step 4.1: Propensity scores}
\begin{itemize}
\item Large scale regularized logistic regression to predict treatment assignment
\item Select covariates from a large set of candidates
\begin{itemize}
\item Demographics
\item All drug exposures
\item All diagnoses, measurements and medical procedures
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org4acb31a}]{Step 4.2: Estimation of treatment effects}
Matching on the propensity score:
\begin{itemize}
\item Form matched sets of patients who share a similar value of the propensity score
\item \alert{\alert{Default}}: Greedy nearest neighbor matching within calipers of the (logit) propensity score
\item \alert{\alert{Other options}}: Many to one matching
\item Derive hazard ratios from Cox proportional hazards model fitted in the matched population
\item Derive absolute risk differences from the difference in Kaplan-Meier estimates at a specific time point of interest
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orgf15ebc8}]{Step 4.2 Estimation of treatment effects}
Inverse probability of treatment weighting
\begin{itemize}
\item Define weights of the form
\end{itemize}
\begin{equation*}
w_i = \frac{T_i}{e_i} + \frac{1-T_i}{1-e_i}
\end{equation*}
\begin{itemize}
\item Derive hazard ratios from weighted Cox proportional hazards model
\item Derive absolute risk reduction from an adjusted Kaplan-Meier estimate for survival in the weighted sample
\end{itemize}
\end{frame}
\section{Application}
\label{sec:org463b04f}
\begin{frame}[label={sec:org439ee6d}]{LEGEND}
\begin{center}
\includegraphics[width=200px]{./img/legendLancet.PNG}
\end{center}
\end{frame}
\begin{frame}[label={sec:orgbfb3aff}]{Problem definition}
Cohorts:
\begin{itemize}
\item \alert{\alert{Treatment}}: Hypertensive patients initiating angiotensine converting enzyme (ACE) inhibitors
\item \alert{\alert{Comparator}}: Hypertensive patients initiating beta blockers
\item \alert{\alert{Outcomes}}: The 55 outcomes of LEGEND
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org106a92e}]{Databases}
\begin{itemize}
\item Truven MarketScan Commercial Claims and Encounters (CCAE)
\begin{itemize}
\item Size > \(140\times10^6\)
\item Enrolees in US employer-sponsored insurance health plans
\end{itemize}
\item Truven MarketScan Medicare Supplemental Beneficiaries (MDCR)
\begin{itemize}
\item Size > \(9\times10^6\)
\item Retirees in the US with primary Medicare supplemental coverage
\end{itemize}
\item Truven MarketScan Multi-state Medicaid (MDCD)
\begin{itemize}
\item Size > \(26\times10^6\)
\item Health insurance claims for Medicaid enrolees
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgfa23485}]{Prediction}
\begin{itemize}
\item Exclude patients with prior outcomes
\item Time at risk is 730 days
\item We use a default set of covariates
\begin{itemize}
\item Demographics
\item All drug exposures
\item All diagnoses, measurements and medical procedures
\item Exclude all concepts used in definitions of treatment and comaparator cohorts
\end{itemize}
\item Use LASSO logistic regression as the prediction algorithm
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orgbccd6ce}]{Estimation}
\begin{itemize}
\item Exclude patients with prior outcomes
\item Exclude patients that initiate both treatments
\item Time at risk is 730 days
\item Use inverse probability of treatment weighting (stabilized weights truncated at the 1st and 99th percentiles) to estimate treatment effects within risk strata
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orge83335a}]{Estimation}
\alert{\alert{Outcome}}: Cardiovascular disease
\tiny
\begin{center}
\begin{tabular}{lllllll}
Database & Treatment & Comparator & Outcomes(T) & Outcomes(C) & Years(T) & Years(C)\\
\hline
CCAE & 765,816 & 383,121 & 16,396 & 10,716 & 2,082,986 & 1,084,545\\
MDCR & 95,402 & 63,594 & 11,525 & 10,248 & 297,749 & 203,272\\
MDCD & 62,385 & 41,289 & 4,928 & 4,200 & 142,921 & 90,773\\
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[label={sec:org709f560}]{Results}
\begin{figure}\centering
\subfloat[MDCD]{
\begin{center}
\includegraphics[width=0.4\textwidth]{./img/psMDCD.jpg}
\end{center}
}\subfloat[CCAE]{
\begin{center}
\includegraphics[width=0.4\textwidth]{./img/psCCAE.jpg}
\end{center}
}\caption{Distribution of the propensity scores within quarters of predicted cardiovascular disease risk}
\end{figure}
\end{frame}

\begin{frame}[label={sec:org81553f6}]{Results}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/lowRisk.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgf97d2e5}]{Results}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/highRisk.png}
\end{center}
\end{frame}
\begin{frame}[label={sec:orga79874f}]{Results}
\href{https://mi-erasmusmc.shinyapps.io/LEGEND-HTE/}{\alert{\alert{Shiny app}}}
\end{frame}

\section{Discussion}
\label{sec:orge38929f}
\begin{frame}[label={sec:org8af0421}]{Conclusion}
\begin{itemize}
\item There exist several approaches for the assessment of HTE
\item A risk modeling approach appears to be more robust to overfitting
\item We have created a standardized framework within OHDSI for risk based assessment of HTE at scale
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org349a79d}]{Future}
\begin{itemize}
\item Complement LEGEND study with risk based assessment of HTE
\item Osteoporosis study
\end{itemize}
\end{frame}
\end{document}
